#include "Patient_Event_Handler.h"

Patient_Event_Handler::Patient_Event_Handler(Reactor *reactor) : reactor_(reactor)
{
}

void Patient_Event_Handler::handle_event(HANDLE h, Event_Type et)
{
	if (et == READ_EVENT)
	{
		char buf[500] = { 0 };
		int result = peer().recv_n(buf, sizeof(buf), 0);

		if (result != SOCKET_ERROR && result>0)
		{
			printf("Received from client:\n");
			for (int i = 0; i < result; i++)
				printf("%c", buf[i]);
			printf("\n\n");
			
			Sleep(10000);
			peer().send_n(buf, result, 0);
			printf("Event handler with handle %i done\n",h);
			reactor_->remove_handler(h, et);
			delete this;
		}
		else
		{
			reactor_->remove_handler(h, et);
			delete this;
		}
	}
}

void Patient_Event_Handler::open()
{
	HANDLE h = this->get_handle();
	reactor_->register_handler(h, this, READ_EVENT);
}


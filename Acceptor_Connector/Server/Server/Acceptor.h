#pragma once
#include "Event_Handler.h"
#include "Reactor.h"

template <class SERVICE_HANDLER, class IPC_ACCEPTOR>
class Acceptor : public Event_Handler {
public:
	Acceptor(const INET_Addr &local_addr, Reactor *r)
	{
		// initialize the IPC_ACCEPTOR to listen for connections
		peer_acceptor_.open(local_addr);
		// register with <reactor>
		reactor->register_handler(this, ACCEPT_MASK);

	}
	virtual void handle_event(HANDLE h, Event_Type et) { accept(); }
protected:
	virtual void accept()
	{
		// S1: GoF: Factory Method � creates a new <SERVICE_HANDLER>
		SERVICE_HANDLER *service_handler = make_service_handler();
		// S2: Hook method that accepts a connection passively
		accept_service_handler(service_handler);
		// S3: Hook method that activates the <SERVICE_HANDLER> by
		// invoking its <open> activation hook method
		activate_service_handler(service_handler);

	}

	virtual SERVICE_HANDLER *make_service_handler();
	virtual void accept_service_handler(SERVICE_HANDLER *sh);
	virtual void activate_service_handler(SERVICE_HANDLER *sh)
	{
		sh->open();
	}
	virtual HANDLE get_handle() const;
private:
	IPC_ACCEPTOR peer_acceptor_; // template placeholder
};


#pragma once
#include "../../Core/Core/Service_Handler.h"
#include "../../Core/Core/SOCK_Stream.h"
#include "../../Core/Core/Reactor.h"
#include <stdio.h>
#include <thread>
using namespace std;

class Patient_Event_Handler : public Peer_Handler
{
public:
	//Status_Handler();
	Patient_Event_Handler(Reactor *r);
	void handle_event(HANDLE h, Event_Type et);


	// Performs handler activation.
	virtual void open();

private:
	Reactor *reactor_;
};


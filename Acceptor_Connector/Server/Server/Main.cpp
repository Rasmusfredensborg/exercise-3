#include <stdio.h>
#include "../../Core/Core/Acceptor.h"
#include "../../Core/Core/Service_Handler.h"
#include "../../Core/Core/Refined_Reactor.h"
#include "../../Core/Core/LF_Thread_Pool.h"
#include "../../Core/Core/LF_Event_Handler.h"
#include <list>
#include "Patient_Event_Handler.h"
#include "Windows.h"

#define STATUS_PORT 8001
#define BULK_DATA_PORT 8002
#define COMMAND_PORT 8003
#define MAX_THREADS 5
typedef Acceptor<Patient_Event_Handler, SOCK_Acceptor> Peer_Acceptor;
timeval tv;
void *worker_thread(void *);

int main()
{
	WSADATA wsaData;
	int iResult;
	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}
	u_short status_port = 8001;
	u_long addr = inet_addr("127.0.0.1");
	INET_Addr status_mINET_Addr(status_port, addr);
	Dispatcher reactor_impl = Dispatcher();
	Refined_Reactor *refined = Refined_Reactor::instance();
	refined->set_reactor_implementor(&reactor_impl);
	LF_Thread_Pool thread_pool(refined);
	Peer_Acceptor acc(status_mINET_Addr, refined,&thread_pool);

	tv.tv_sec = 2;
	tv.tv_usec = 500;
	
	list<thread*> threads;

	for (int i = 0; i < MAX_THREADS; i++)
	{
		thread* t1 = new thread(worker_thread, &thread_pool);
		threads.push_back(t1);
	}	thread_pool.join(&tv);	
	while (1);
}

void *worker_thread(void *arg) {
	LF_Thread_Pool *thread_pool = static_cast <LF_Thread_Pool *> (arg);
	thread_pool->join(&tv);
	return 0;
};
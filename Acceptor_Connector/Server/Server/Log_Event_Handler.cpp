#include "Log_Event_Handler.h"

Log_Event_Handler::Log_Event_Handler(const SOCK_Stream &stream, Reactor *reactor) : peer_stream_(stream), reactor_(reactor)
{
	HANDLE h = this->get_handle();
	reactor_->register_handler(h, this, READ_EVENT);
}

void Log_Event_Handler::handle_event(HANDLE h, Event_Type et)
{
	if (et == READ_EVENT)
	{
		char buf[500] = { 0 };
		int result = peer_stream_.recv_n(buf, sizeof(buf), 0);

		if (result != SOCKET_ERROR && result>0)
		{
			printf("Received from LOG:\n");
			for (int i = 0; i < result; i++)
				printf("%c", buf[i]);
			printf("\n\n");
		}
		else
		{
			reactor_->remove_handler(this, READ_EVENT);
			delete this;
		}
	}
}

HANDLE Log_Event_Handler::get_handle() const
{
	return (HANDLE)peer_stream_.get_handle();
}

#include "Alarm_Event_Acceptor.h"



Alarm_Event_Acceptor::Alarm_Event_Acceptor(INET_Addr &addr, Reactor *reactor) : acceptor_(addr), reactor_(reactor)
{
	HANDLE h = this->get_handle();
	reactor_->register_handler(h, this, ACCEPT_EVENT);
}

void Alarm_Event_Acceptor::handle_event(HANDLE h, Event_Type et)
{
	if (et == ACCEPT_EVENT)
	{
		SOCK_Stream client_connection((SOCKET)h);
		acceptor_.accept_socket(client_connection);

		//Alarm_Event_Handler *handler = new Alarm_Event_Handler(client_connection, reactor_);
	}
}

HANDLE Alarm_Event_Acceptor::get_handle() const
{
	return (HANDLE)acceptor_.get_handle();
}

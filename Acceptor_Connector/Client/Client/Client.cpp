
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <Windows.h>
#include <stdio.h>
#include "../../Core/Core/Connector.h"
#include "../../Core/Core/Service_Handler.h"
#include "../../Core/Core/Refined_Reactor.h"
#include "Patient_Event_Router.h"
#include <vector>
#include <iostream>
#include <string.h>
#include <sstream>
#include <stdlib.h>

using namespace std;

#define SD_SEND 1
#define DEFAULT_PORT 8001
#define DEFAULT_BUFLEN 512

typedef Connector<Peer_Router, SOCK_Connector> Peer_Connector;typedef vector<Peer_Router*>::iterator Peer_Iterator;
void get_peer_addrs(vector<Peer_Router*> &peers);

int main() {
	WSADATA wsaData;
	int iResult;
	
	char *sendbuf = "this is client #1";
	char recvbuf[DEFAULT_BUFLEN];
	int recvbuflen = DEFAULT_BUFLEN;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	u_short peer_port = DEFAULT_PORT;
	u_long addr = inet_addr("127.0.0.1");
	INET_Addr mINET_Addr(peer_port, addr);
	Dispatcher reactor_impl = Dispatcher();
	Refined_Reactor *refined = Refined_Reactor::instance();
	refined->set_reactor_implementor(&reactor_impl);
	Peer_Connector peer_connector(refined);
	
	Patient_Event_Router *status_router = new Patient_Event_Router(refined);// = new Status_Router(mINET_Addr);
	Patient_Event_Router *status_router2 = new Patient_Event_Router(refined);// = new Status_Router(mINET_Addr);

	vector<Peer_Router*> peers;
	get_peer_addrs(peers);
	//Patient_Event_Connector patient_connector = Patient_Event_Connector(patient_mINET_Addr, refined);
	//Log_Event_Connector log_connector = Log_Event_Connector(log_mINET_Addr, refined);

	struct timeval tv;
	tv.tv_sec = 2;
	tv.tv_usec = 500000;

	/*for (Peer_Iterator peer = peers.begin(); peer != peers.end(); peer++) 
	{
		peer_connector.connect((*peer), (*peer)-> remote_addr(),	Peer_Connector::SYNC);
	}*/

	peer_connector.connect(status_router, mINET_Addr, Peer_Connector::SYNC);
	//peer_connector.connect(status_router2, mINET_Addr, Peer_Connector::SYNC);

	for (; ;)
		refined->handle_events();
}

void get_peer_addrs(vector<Peer_Router*> &peers)
{
	const char config[] = "ip=127.0.0.1\n""status=27015\n""data=27016\n""command=27017\n";

	istringstream is_file(config);
	u_long addr = 0;
	string line;
	while (getline(is_file, line))
	{
		istringstream is_line(line);
		string key;
		if (getline(is_line, key, '='))
		{
			string value;
			getline(is_line, value);
			if(!key.compare("ip"))
				addr = inet_addr(value.c_str());
			
			if (!key.compare("status"))
			{
				int val = stoi(value);
				INET_Addr mINET_Addr(stoi(value), addr);
				//Status_Router *status_router = new Status_Router(mINET_Addr);
				
				//peers.push_back(status_router);
			}

			if (!key.compare("data"))
			{
				INET_Addr mINET_Addr(stoi(value), addr);
				//Bulk_Data_Router *status_router = new Bulk_Data_Router(mINET_Addr);

				//peers.push_back(status_router);
			}

			if (!key.compare("command"))
			{
				INET_Addr mINET_Addr(stoi(value), addr);
				//Status_Router *status_router = new Status_Router(mINET_Addr);

				//peers.push_back(status_router);
			}
		}
	}
}
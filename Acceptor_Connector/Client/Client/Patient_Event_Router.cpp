#include "Patient_Event_Router.h"
#include <string>
using namespace std;
Patient_Event_Router::Patient_Event_Router(Reactor *reactor) : reactor_(reactor)
{}

void Patient_Event_Router::handle_event(HANDLE h, Event_Type et)
{
	if (et == WRITE_EVENT)
	{
		string to_server = "Client #" + to_string((int)h);
		const char *buf = to_server.c_str();
		
		int iResult = peer().send_n(buf, (int)strlen(buf), 0);
		if (iResult == SOCKET_ERROR) {
			printf("send failed with error: %d\n", WSAGetLastError());
			peer().close_socket();
			WSACleanup();
			reactor_->remove_handler(this, WRITE_EVENT);
			delete this;
			return;
		}
		else
		{
			char recbuf[500] = { 0 };
			int result = peer().recv_n(recbuf, sizeof(recbuf), 0);
			for (int i = 0; i < result; i++)
				printf("%c", recbuf[i]);
			peer().close_socket();
			reactor_->remove_handler(this, WRITE_EVENT);
			delete this;
			return;
		}
		
	}
}

void Patient_Event_Router::open()
{
	HANDLE h = this->get_handle();
	reactor_->register_handler(h, this, WRITE_EVENT);
}

#pragma once
#include "../../Core/Core/Service_Handler.h"
#include "../../Core/Core/Reactor.h"
#include <stdio.h>

class Patient_Event_Router : public Peer_Router
{
public:
	Patient_Event_Router(Reactor *reactor);
	void handle_event(HANDLE h, Event_Type et);
	virtual void open();
private:
	Reactor *reactor_;
};


#pragma once

#include <Windows.h>
#include "INET_Addr.h"
#include "SOCK_Stream.h"

class SOCK_Acceptor {
public:
	// Initialize a passive-mode acceptor socket.
	SOCK_Acceptor();
	SOCK_Acceptor(const INET_Addr &addr);
	void open(const INET_Addr &sock_addr);
	void accept_socket(SOCK_Stream &s);
	SOCKET get_handle() const;
private:
	SOCKET handle_; // Socket handle factory.
};

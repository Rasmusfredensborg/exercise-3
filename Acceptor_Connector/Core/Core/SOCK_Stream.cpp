
#include "SOCK_Stream.h"

#include <stdio.h>
#include <iostream>

using namespace std;

SOCK_Stream::SOCK_Stream() : handle_(INVALID_SOCKET)
{

}

SOCK_Stream::SOCK_Stream(SOCKET h)
{
	set_handle(h);
}

SOCK_Stream::~SOCK_Stream()
{
	//closesocket(handle_);
}

int SOCK_Stream::close_socket()
{
	int result = closesocket(handle_);
	if (result == SOCKET_ERROR) {
		printf("close_socket failed with error: %d\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}
	return result;
}

// Set/get the underlying SOCKET handle.
void SOCK_Stream::set_handle(SOCKET h)
{
	handle_ = h;
}

SOCKET SOCK_Stream::get_handle() const
{
	return handle_;
}

// I/O operations for "short" receives and sends.
size_t SOCK_Stream::recv_n(char *buf, size_t len, int flags)
{
	return recv(handle_, buf, len, flags);
};

size_t SOCK_Stream::send_n(const char *buf, size_t len, int flags)
{
	int iSendResult = send(handle_, buf, len, flags);

	if (iSendResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}
	printf("Bytes sent: %d\n", iSendResult);

	return iSendResult;
};

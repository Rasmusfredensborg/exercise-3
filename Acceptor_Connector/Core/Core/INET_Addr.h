#pragma once

#include <Windows.h>

class INET_Addr {
public:
	INET_Addr();
	INET_Addr(u_short port, u_long addr);
	u_short get_port() const;
	u_long get_ip_addr() const;
	const sockaddr *addr() const;
	size_t size() const;
private:
	sockaddr_in addr_;
};
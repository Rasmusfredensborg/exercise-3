#include "Demux_Table.h"

void Demux_Table::add_tuple(HANDLE h, Tuple tuple)
{
	TupleMap::iterator it = tuple_Table_.find(h);

	/*if (it != tuple_Table_.end())
	{
		it->second.event_type_ = it->second.event_type_ | tuple.event_type_;
	}
	else
	{*/
		tuple_Table_[h] = tuple;
	//}
}

bool Demux_Table::remove_tuple(HANDLE h, Event_Type et)
{
	TupleMap::iterator it = tuple_Table_.find(h);
	if (it != tuple_Table_.end())
	{
		if (it->second.event_type_ == et)
		{
			tuple_Table_.erase(it);
			return true;
		}
		return true;
	}
	else
	{
		return false;
	}
}

Demux_Table::Tuple* Demux_Table::get_tuple_(HANDLE h)
{
	TupleMap::iterator it = tuple_Table_.find(h);
	if (it != tuple_Table_.end())
	{
		return &(it->second);
	}
	return NULL;
}

void Demux_Table::convert_to_fd_sets(fd_set* read_fds, fd_set* write_fds, fd_set* except_fds) 
{
	FD_ZERO(read_fds); 
	FD_ZERO(write_fds);
	FD_ZERO(except_fds);

	TupleMap tuple_clone = tuple_Table_;

	TupleMap::iterator it = tuple_clone.begin();
	
	for (; it != tuple_clone.end(); it++)
	{
		if (it->second.event_type_ == READ_EVENT || it->second.event_type_ == ACCEPT_EVENT)
		{
			FD_SET((SOCKET)it->first, read_fds);
		};
		if (it->second.event_type_ == CONNECT_EVENT)
		{
			FD_SET((SOCKET)it->first, write_fds);
		};
	}
}
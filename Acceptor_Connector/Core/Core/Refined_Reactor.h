#pragma once
#include "Reactor.h"
#include "Dispatcher.h"
#include <stdio.h>
class Refined_Reactor :	public Reactor
{
public:
	//Refined_Reactor(Reactor_Implementation *ri);
	void register_handler(Event_Handler *eh, Event_Type et);
	void register_handler(HANDLE h, Event_Handler *eh, Event_Type et);
	void remove_handler(Event_Handler *eh, Event_Type et);
	void remove_handler(HANDLE h, Event_Type et);
	// Entry point into the reactive event loop 
	int handle_events(TIMEVAL *timeout = 0);
	void set_reactor_implementor(Reactor_Implementation *ri);
	void deactivate_handle(HANDLE, Event_Type et);
	void reactivate_handle(HANDLE, Event_Type et);

	// Define a singleton access point (GoF pattern)
	static Refined_Reactor *instance();

private:
	static Refined_Reactor* pInstance;
};
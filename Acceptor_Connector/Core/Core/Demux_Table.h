#pragma once
#include "Event_Handler.h"
#include <Windows.h>
#include <vector>
#include <algorithm>
#include <map>
using std::map;

class Demux_Table
{
public:
	void convert_to_fd_sets(fd_set* read_fds, fd_set* write_fds, fd_set* except_fds);

	struct Tuple
	{
		Event_Handler *event_handler_;
		Event_Type event_type_;
		bool operator==(const Tuple &  rhs) const { return this == &rhs; }
		Tuple(Event_Handler *eh, Event_Type et)
		{
			event_handler_ = eh;
			event_type_ = et;
		}
		Tuple(const Tuple& tuple)
		{
			event_handler_ = tuple.event_handler_;
			event_type_ = tuple.event_type_;
		}
		Tuple()
		{
			event_handler_ = NULL;
			event_type_ = ACCEPT_EVENT;
		}
		Tuple& operator=(const Tuple& tuple)
		{
			event_handler_ = tuple.event_handler_;
			event_type_ = tuple.event_type_;
			return *this;
		}
	};
	
	void add_tuple(HANDLE h, Tuple tuple);
	bool remove_tuple(HANDLE h, Event_Type et);
	Tuple* get_tuple_(HANDLE h);
	typedef map<HANDLE, Tuple> TupleMap;	
	TupleMap tuple_Table_;
};


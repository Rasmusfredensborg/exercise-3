#pragma once

#include <Windows.h>
#include "INET_Addr.h"
#include "SOCK_Stream.h"


class SOCK_Connector {
public:
	SOCK_Connector();
	SOCK_Connector(INET_Addr &addr);
	int connect_socket(const INET_Addr & addr, SOCK_Stream & s);
	int connect_socket(SOCK_Stream & s);
	SOCKET get_handle() const;
private:
	SOCKET handle_;
	INET_Addr &addr_;
	SOCK_Stream stream_;
};

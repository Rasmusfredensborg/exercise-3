#include "SOCK_Acceptor.h"

#include <iostream>
#include "SOCK_Acceptor.h"

SOCK_Acceptor::SOCK_Acceptor()
{
}
// Initialize a passive-mode acceptor socket.
SOCK_Acceptor::SOCK_Acceptor(const INET_Addr &addr) {

	// Create a SOCKET for connecting to server
	handle_ = socket(PF_INET, SOCK_STREAM, 0);
	if (handle_ == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		WSACleanup();
		exit(EXIT_FAILURE);
	}
};

void SOCK_Acceptor::open(const INET_Addr &sock_addr)
{
	handle_ = socket(PF_INET, SOCK_STREAM, 0);
	if (handle_ == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		WSACleanup();
		exit(EXIT_FAILURE);
	}
	// Setup the TCP listening socket
	int result = bind(handle_, sock_addr.addr(), sock_addr.size());
	if (result == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		closesocket(handle_);
		WSACleanup();
		exit(EXIT_FAILURE);
	}

	result = listen(handle_, 5); //SOMAXCONN
	if (result == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(handle_);
		WSACleanup();
		exit(EXIT_FAILURE);
	}
	else {
		printf("Listening for clients...\n");
	}
}
// Accept a connection and initialize the < stream > .
void SOCK_Acceptor::accept_socket(SOCK_Stream &s) {

	// Accept the connection.
	s.set_handle(accept(handle_, NULL, NULL));
	
	//if (s.get_handle == INVALID_SOCKET) {
	//	printf("accept failed with error: %ld\n", WSAGetLastError());
	//	closesocket(&s.get_handle);
	//	WSACleanup();
	//	return;
	//}

	printf("Client connected!\n");

}

SOCKET SOCK_Acceptor::get_handle() const
{
	return handle_;
}

#pragma once
#pragma once
#include <windows.h>
#include "../../Core/Core/INET_Addr.h"
#include "../../Core/Core/SOCK_Acceptor.h"
#include "../../Core/Core/SOCK_Connector.h"
#include "../../Core/Core/SOCK_Stream.h"
typedef unsigned int Event_Type;


enum Event_Types {
	INACTIVE = -1,
	READ_EVENT = 01,
	ACCEPT_EVENT = 01,
	WRITE_EVENT = 02,
	CONNECT_EVENT = 02,
	TIMEOUT_EVENT = 04
};

class Event_Handler
{
public:
	virtual void handle_event(HANDLE h, Event_Type et) = 0;
	virtual HANDLE get_handle() const = 0;
};
#pragma once
#include "LF_Event_Handler.h"
#include "Reactor.h"
#include <vector>
#include <algorithm>
#include <map>
using std::map;

template <class SERVICE_HANDLER, class IPC_CONNECTOR>
class Connector : public Event_Handler
{
public:
	enum Connection_Mode { SYNC, ASYNC };
	Connector(Reactor *reactor) : reactor_(reactor)
	{
	}
	// Template Method
	void connect(SERVICE_HANDLER *sh, const INET_Addr &remote_addr, Connection_Mode mode)
	{
		connect_service_handler(sh, remote_addr, mode);
	}
	// Adapter Method (only used in asynchronous mode)
	virtual void handle_event(HANDLE handle, Event_Type et)
	{
		//complete(handle);
	}
protected:
	//virtual void complete(HANDLE handle)	//{
	//	Connection_Map::iterator i = connection_map_.find(handle);
	//	if (i == connection_map_.end())
	//		throw std::runtime_error("connector::complete() failed.");
	//	SERVICE_HANDLER *svc_handler = (*i).second;
	//	reactor_->remove_handler(handle);
	//	connection_map_.erase(i);
	//	// set connection back to blocking
	//	svc_handler->peer().set_blocking();
	//	// connection is complete so activate handler
	//	activate_service_handler(svc_handler); // calls open() hook
		virtual void connect_service_handler(SERVICE_HANDLER *svc_handler, const INET_Addr &addr, Connection_Mode mode)
	{
		if (mode == SYNC)
		{ // Modified version in relation to the POSA2 book
			connector_.connect_socket (addr,svc_handler->peer());
			// activate if we connect synchronously
			activate_service_handler(svc_handler); // calls open() hook
			HANDLE temp = svc_handler->get_handle();
		}
		else 
		{ // Asynchronous connect case (sets connection to nonblocking)
			//connector_.connectAsync(svc_handler->peer(), addr);
			//HANDLE myAsyncHandle = svc_handler->get_handle();
			//// register for asynchronously call back
			//reactor_->register_handler(myAsyncHandle);
			//// store <service handler *> in map
			//connection_map_[myAsyncHandle] = svc_handler;
		}
	}
	virtual void activate_service_handler(SERVICE_HANDLER *sh)
	{
		sh->open();
	}
	virtual HANDLE get_handle() const
	{
		return (HANDLE)connector_.get_handle();
	}
private:
	IPC_CONNECTOR connector_;
	typedef map<HANDLE, SERVICE_HANDLER*> Connection_Map;
	Connection_Map connection_map_;
	Reactor *reactor_;
};

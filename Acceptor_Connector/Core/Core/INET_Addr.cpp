
#include <iostream>
#include "INET_Addr.h"

INET_Addr::INET_Addr()
{}

INET_Addr::INET_Addr(u_short port, u_long addr) 
{
	// Set up the address to become a server.
	memset(&addr_, 0, sizeof addr_);
	addr_.sin_family = PF_INET;
	addr_.sin_port = htons(port);
	addr_.sin_addr.s_addr = addr;

	printf("INET_Addr, family: %u\n", addr_.sin_family);
	printf("INET_Addr, port: %u\n", get_port());
	printf("INET_Addr, addr: %u\n", get_ip_addr());
}

u_short INET_Addr::get_port() const
{
	return ntohs(addr_.sin_port);
}

u_long INET_Addr::get_ip_addr() const
{
	return ntohl(addr_.sin_addr.s_addr);
}

 const sockaddr *INET_Addr::addr() const
{
	return reinterpret_cast <const sockaddr *>(&addr_);
}

size_t INET_Addr::size() const 
{
	return sizeof(addr_);
}

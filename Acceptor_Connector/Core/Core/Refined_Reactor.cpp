#include "Refined_Reactor.h"

void Refined_Reactor::register_handler(Event_Handler *eh, Event_Type et)
{
	reactor_impl_->register_handler(eh, et);
}

void Refined_Reactor::register_handler(HANDLE h, Event_Handler *eh, Event_Type et)
{
	reactor_impl_->register_handler(h, eh, et);
}

void Refined_Reactor::remove_handler(Event_Handler *eh, Event_Type et)
{
	reactor_impl_->remove_handler(eh, et);
}

void Refined_Reactor::remove_handler(HANDLE h, Event_Type et)
{
	reactor_impl_->remove_handler(h, et);
}

int Refined_Reactor::handle_events(TIMEVAL *timeout)
{
	return reactor_impl_->handle_events(timeout);
}

Refined_Reactor *Refined_Reactor::instance()
{
	if (pInstance == NULL)
	{
		pInstance = new Refined_Reactor;
	}
	return pInstance;
}

void Refined_Reactor::set_reactor_implementor(Reactor_Implementation *ri)
{
	if (ri != NULL)
		reactor_impl_ = ri;
	else
		printf("Could not set implementor\n");
}

void Refined_Reactor::deactivate_handle(HANDLE h, Event_Type et)
{
	reactor_impl_->deactivate_handle(h, et);
}
void Refined_Reactor::reactivate_handle(HANDLE h, Event_Type et)
{
	reactor_impl_->reactivate_handle(h, et);
}
Refined_Reactor* Refined_Reactor::pInstance = NULL;
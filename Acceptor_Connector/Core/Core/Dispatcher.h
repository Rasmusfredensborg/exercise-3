#pragma once
#include "Reactor_Implementation.h"
#include "Demux_Table.h"

#define MAX_NO_OF_HANDLES 5

class Dispatcher : public Reactor_Implementation
{
public:
	Dispatcher();
	~Dispatcher();
	void register_handler(Event_Handler *eh, Event_Type et);
	void register_handler(HANDLE h, Event_Handler *eh, Event_Type et);
	void remove_handler(Event_Handler *eh, Event_Type et);
	void remove_handler(HANDLE h, Event_Type et);
	void deactivate_handle(HANDLE, Event_Type et);
	void reactivate_handle(HANDLE, Event_Type et);
	// Entry point into the reactive event loop 
	int handle_events(TIMEVAL *timeout = 0);
private:
	Demux_Table demux_table_;
	Event_Handler *deactivated_handler_;
};
#pragma once
#include "Event_Handler.h"
#include <stdio.h>
template <class IPC_STREAM>
class Service_Handler : public Event_Handler {
public:
	virtual void handle_event(HANDLE h, Event_Type et) = 0;
	// Hook template method, defined by a subclass
	virtual void open() = 0;
	IPC_STREAM &peer() { return ipc_stream_; }
	HANDLE get_handle() const { return (HANDLE)ipc_stream_.get_handle(); }
	void set_handle(HANDLE handle) { ipc_stream_.set_handle((SOCKET)handle); }
	//virtual INET_Addr remote_addr() = 0;
private:
	// Template placeholder for a concrete IPC mechanism wrapper
	// fa�ade, which encapsulate a data-mode transport endpoint and
	// transport handle
	IPC_STREAM ipc_stream_;
};

typedef Service_Handler<SOCK_Stream> Peer_Router;
typedef Service_Handler<SOCK_Stream> Peer_Handler;
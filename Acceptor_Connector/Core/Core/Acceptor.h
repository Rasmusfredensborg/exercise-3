#pragma once
#pragma once
#include "Service_Handler.h"
#include "LF_Event_Handler.h"
#include "LF_Thread_Pool.h"
#include "Reactor.h"

template <class SERVICE_HANDLER, class IPC_ACCEPTOR>
class Acceptor : public Event_Handler {
public:
	Acceptor(const INET_Addr &local_addr, Reactor *r, LF_Thread_Pool *tp)
	{
		r_ = r;
		tp_ = tp;
		// initialize the IPC_ACCEPTOR to listen for connections
		peer_acceptor_.open(local_addr);
		// register with <reactor>
		r_->register_handler(this, ACCEPT_EVENT);
	}
	virtual void handle_event(HANDLE h, Event_Type et) { accept(); }
protected:
	virtual void accept()
	{
		// S1: GoF: Factory Method � creates a new <SERVICE_HANDLER>
		LF_Event_Handler *service_handler = make_service_handler();
		// S2: Hook method that accepts a connection passively
		accept_service_handler(service_handler);
		// S3: Hook method that activates the <SERVICE_HANDLER> by
		// invoking its <open> activation hook method
		activate_service_handler(service_handler);		
	}

	virtual LF_Event_Handler *make_service_handler()
	{
		SERVICE_HANDLER *concrete_service_handler = new SERVICE_HANDLER(r_);
		return new LF_Event_Handler(concrete_service_handler,tp_,r_);
	}

	virtual void accept_service_handler(LF_Event_Handler *sh)
	{
		SOCK_Stream client_connection(peer_acceptor_.get_handle());
		peer_acceptor_.accept_socket(client_connection);
		sh->set_handle((HANDLE)client_connection.get_handle());
	}
	virtual void activate_service_handler(LF_Event_Handler *sh)
	{
		sh->open();
		tp_->promote_new_leader();
	}
	virtual HANDLE get_handle() const
	{
		return (HANDLE)peer_acceptor_.get_handle();
	}
private:
	IPC_ACCEPTOR peer_acceptor_; // template placeholder
	Reactor *r_;
	LF_Thread_Pool *tp_;
};


//Acceptor<class SERVICE_HANDLER, class IPC_ACCEPTOR>::Acceptor()
//{}
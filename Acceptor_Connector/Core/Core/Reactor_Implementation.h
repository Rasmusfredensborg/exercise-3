#pragma once
#include "Reactor.h"
#include <stdio.h>
class Reactor_Implementation
{
public:
	Reactor_Implementation();
	virtual void register_handler(Event_Handler *eh, Event_Type et)=0;
	virtual void register_handler(HANDLE h, Event_Handler *eh, Event_Type et) = 0;
	virtual void remove_handler(Event_Handler *eh, Event_Type et)=0;
	virtual void remove_handler(HANDLE h, Event_Type et) = 0;
	virtual void deactivate_handle(HANDLE, Event_Type et) = 0;
	virtual void reactivate_handle(HANDLE, Event_Type et) = 0;
	// Entry point into the reactive event loop 
	virtual int handle_events(TIMEVAL *timeout = 0)=0;
};
#pragma once
#include "Reactor.h"
#include <thread>
#include <mutex>
#include <condition_variable>
#include <iostream>
using namespace std;

class LF_Thread_Pool {
public:
	LF_Thread_Pool(Reactor *r);
	~LF_Thread_Pool();
	void promote_new_leader();
	void join(TIMEVAL* timeout = 0);
	void deactivate_handle(HANDLE, Event_Type et);
	void reactivate_handle(HANDLE, Event_Type et);
private:
	Reactor *reactor_;
	mutex leader_mutex_;
	size_t leader_thread_;
	condition_variable followers_condition_;
	std::hash<thread::id> hasher;
};

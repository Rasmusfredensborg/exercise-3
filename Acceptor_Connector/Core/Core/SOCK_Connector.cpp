
#include <iostream>
#include "SOCK_Connector.h"

SOCK_Connector::SOCK_Connector() : addr_(INET_Addr())
{
}

SOCK_Connector::SOCK_Connector(INET_Addr &addr) : addr_(addr)
{
	// Create a SOCKET for connecting to server
	handle_ = socket(PF_INET, SOCK_STREAM, 0);
	if (handle_ == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		WSACleanup();
		exit(EXIT_FAILURE);
	}
}

int SOCK_Connector::connect_socket(const INET_Addr &addr, SOCK_Stream &s)
{
	handle_ = socket(PF_INET, SOCK_STREAM, 0);
	if (handle_ == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		WSACleanup();
		exit(EXIT_FAILURE);
	}
	s.set_handle(handle_);
	// Connect to server.
	int iResult = connect(s.get_handle(), addr.addr(), addr.size());
	if (iResult == SOCKET_ERROR) {
		printf("connect failed with error: %ld\n", WSAGetLastError());
		s.close_socket();
	}
	return iResult;
}

int SOCK_Connector::connect_socket(SOCK_Stream &s)
{
	//s.set_handle(handle_);
	// Connect to server.
	printf("%d, %d, %d", s.get_handle(), addr_.addr(), addr_.size());
	int iResult = connect(s.get_handle(), addr_.addr(), addr_.size());
	if (iResult == SOCKET_ERROR) {
		printf("connect failed with error: %ld\n", WSAGetLastError());
		s.close_socket();
	}
	return iResult;
}

SOCKET SOCK_Connector::get_handle() const
{
	return handle_;
}
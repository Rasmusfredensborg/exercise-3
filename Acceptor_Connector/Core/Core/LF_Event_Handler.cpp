#include "LF_Event_Handler.h"

LF_Event_Handler::LF_Event_Handler(Peer_Handler *eh, LF_Thread_Pool *tp, Reactor *r) : reactor_(r), concrete_event_handler_(eh), thread_pool_(tp)
{}

void LF_Event_Handler::handle_event(HANDLE h, Event_Type et)
{
	thread_pool_->deactivate_handle(h, et);
	thread_pool_->promote_new_leader();
	concrete_event_handler_->handle_event(h, et);
	thread_pool_->reactivate_handle(h, et);
	
	reactor_->remove_handler(h, et);
}

void LF_Event_Handler::set_handle(HANDLE h)
{
	concrete_event_handler_->set_handle(h);
}

void LF_Event_Handler::open()
{
	HANDLE h = concrete_event_handler_->get_handle();
	reactor_->register_handler(h, this, READ_EVENT);
}

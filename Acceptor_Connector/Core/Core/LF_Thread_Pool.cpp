#include "LF_Thread_Pool.h"

#define NO_CURRENT_LEADER 0
LF_Thread_Pool::LF_Thread_Pool(Reactor *r) : reactor_(r) { }

LF_Thread_Pool::~LF_Thread_Pool()
{
	leader_mutex_.unlock();
}


void LF_Thread_Pool::promote_new_leader()
{
	unique_lock<mutex> lock(leader_mutex_);
	if (leader_thread_ != hasher(this_thread::get_id()))
		return;
	leader_thread_ = NO_CURRENT_LEADER;
	followers_condition_.notify_one();
}

void LF_Thread_Pool::join(TIMEVAL* timeout)
{
	unique_lock<mutex> lock(leader_mutex_);
	while (1)
	{
		while(leader_thread_!=NO_CURRENT_LEADER)
			followers_condition_.wait(lock);
		leader_thread_ = hasher(this_thread::get_id());
		cout << "Current leader: " << this_thread::get_id() << "\n";
		lock.unlock();
		while(reactor_->handle_events(timeout)==0);
		lock.lock();
	}
}

void LF_Thread_Pool::deactivate_handle(HANDLE h, Event_Type et)
{
	reactor_->deactivate_handle(h, et);
	
}

void LF_Thread_Pool::reactivate_handle(HANDLE h, Event_Type et)
{
	reactor_->reactivate_handle(h, et);
}
#pragma once
#include "Event_Handler.h"
#include "Service_Handler.h"
#include "LF_Thread_Pool.h"

class LF_Event_Handler : public Peer_Handler
{
public:
	LF_Event_Handler(Peer_Handler *eh, LF_Thread_Pool *tp, Reactor *r);
	virtual void handle_event(HANDLE h, Event_Type et);
	void set_handle(HANDLE h);
	void open();
private:
	Peer_Handler * concrete_event_handler_;
	LF_Thread_Pool * thread_pool_;
	Reactor *reactor_;
};
#include "Dispatcher.h"

void Dispatcher::register_handler(HANDLE h, Event_Handler *eh, Event_Type et)
{
	//HANDLE handle = eh->get_handle();
	Demux_Table::Tuple tuple(eh, et);
	demux_table_.add_tuple(h, tuple);
	//printf("Hi from register_handler select reactor implementation\n");

}

void Dispatcher::register_handler(Event_Handler *eh, Event_Type et)
{
	Demux_Table::Tuple tuple(eh, et);
	demux_table_.add_tuple(eh->get_handle(), tuple);
	//printf("Hi from register_handler select reactor implementation\n");

}

void Dispatcher::remove_handler(HANDLE h, Event_Type et)
{
	demux_table_.remove_tuple(h, et);
	return;
}

void Dispatcher::remove_handler(Event_Handler *eh, Event_Type et)
{
	HANDLE h = eh->get_handle();
	demux_table_.remove_tuple(h, et);
	return;
}

int Dispatcher::handle_events(TIMEVAL *timeout)
{
	fd_set read_fds, write_fds, except_fds;

	demux_table_.convert_to_fd_sets(&read_fds, &write_fds, &except_fds);
	HANDLE max_handle = HANDLE(MAX_NO_OF_HANDLES);
	int nResult = 0;
	nResult = select((int)max_handle + 1, &read_fds, &write_fds, &except_fds, timeout);
	if (nResult <= 0)
	{
		return nResult;
	}
	
	Demux_Table demux_clone = demux_table_;

	Demux_Table::TupleMap::iterator it = demux_clone.tuple_Table_.begin();
	for (; it != demux_clone.tuple_Table_.end(); it++)
	{
		int table_size = demux_clone.tuple_Table_.size();
		if (FD_ISSET(it->first, &read_fds))
		{
			Demux_Table::Tuple* pTuple = demux_clone.get_tuple_(it->first);

			if (pTuple->event_type_ == ACCEPT_EVENT || pTuple->event_type_ == READ_EVENT)
			{
				pTuple->event_handler_->handle_event(it->first, ACCEPT_EVENT);
				break;
			};
		}
		if (FD_ISSET(it->first, &write_fds))
		{
			Demux_Table::Tuple* pTuple = demux_clone.get_tuple_(it->first);
			if (pTuple->event_type_ == CONNECT_EVENT)
			{
				pTuple->event_handler_->handle_event(it->first, CONNECT_EVENT);
				break;
			};
		}
		if (FD_ISSET(it->first, &except_fds))
		{
			Demux_Table::Tuple* pTuple = demux_clone.get_tuple_(it->first);
			if (pTuple->event_type_ == TIMEOUT_EVENT)
				pTuple->event_handler_->handle_event(it->first, TIMEOUT_EVENT);
			break;
		}
	}
	return nResult;
}

void Dispatcher::deactivate_handle(HANDLE h, Event_Type et)
{
	demux_table_.get_tuple_(h)->event_type_=INACTIVE;
}

void Dispatcher::reactivate_handle(HANDLE h, Event_Type et)
{
	if(demux_table_.get_tuple_(h)!=NULL)
		demux_table_.get_tuple_(h)->event_type_ = et;
}

Dispatcher::Dispatcher()
{
}


Dispatcher::~Dispatcher()
{
}
